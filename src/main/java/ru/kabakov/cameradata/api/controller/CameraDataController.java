package ru.kabakov.cameradata.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.kabakov.cameradata.api.converter.CameraResDataDTOListToResponseConverter;
import ru.kabakov.cameradata.api.json.CameraResDataGetListResponse;
import ru.kabakov.cameradata.service.CameraDataService;
import ru.kabakov.cameradata.service.dto.CameraResDataDTO;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequiredArgsConstructor
public class CameraDataController {
    public final CameraDataService cameraDataService;
    public final CameraResDataDTOListToResponseConverter cameraResDataDTOListToResponseConverter;

    @GetMapping("/getCameraResData")
    public @ResponseBody
    ResponseEntity<CameraResDataGetListResponse> getCameraResDataList() {
        List<CameraResDataDTO> cameraResDataDTOList = cameraDataService.getCameraResData();
        if (cameraResDataDTOList == null) {
            return status(HttpStatus.BAD_REQUEST).build();
        }
        if (cameraResDataDTOList.isEmpty()) {
            return status(HttpStatus.NO_CONTENT).build();
        }

        return ok(cameraResDataDTOListToResponseConverter.convert(cameraResDataDTOList));
    }
}
