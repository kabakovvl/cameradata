package ru.kabakov.cameradata.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.kabakov.cameradata.service.dto.CameraResDataDTO;

import java.util.List;

@Data
@AllArgsConstructor
public class CameraResDataGetListResponse {
    private List<CameraResDataDTO> cameraResDataDTOList;
}
