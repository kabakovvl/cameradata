package ru.kabakov.cameradata.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.kabakov.cameradata.api.json.CameraResDataGetListResponse;
import ru.kabakov.cameradata.service.dto.CameraResDataDTO;

import java.util.List;

@Component
public class CameraResDataDTOListToResponseConverter implements Converter<List<CameraResDataDTO>, CameraResDataGetListResponse> {
    @Override
    public CameraResDataGetListResponse convert(List<CameraResDataDTO> cameraResDataDTO) {
        return new CameraResDataGetListResponse(cameraResDataDTO);
    }
}
