package ru.kabakov.cameradata;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@RequiredArgsConstructor
@SpringBootApplication
public class CameraDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(CameraDataApplication.class, args);
    }
}
