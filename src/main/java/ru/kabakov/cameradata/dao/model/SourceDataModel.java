package ru.kabakov.cameradata.dao.model;

import lombok.Data;

@Data
public class SourceDataModel {
    private UrlType urlType;
    private String videoUrl;
}
