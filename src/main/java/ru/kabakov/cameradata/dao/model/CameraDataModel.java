package ru.kabakov.cameradata.dao.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CameraDataModel {
    private Long id;
    private String sourceDataUrl;
    private String tokenDataUrl;
}