package ru.kabakov.cameradata.dao.model;

import lombok.Data;

@Data
public class TokenDataModel {
    private String value;
    private Integer ttl;
}
