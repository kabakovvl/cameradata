package ru.kabakov.cameradata.dao.model;

public enum UrlType {
    LIVE,
    ARCHIVE
}
