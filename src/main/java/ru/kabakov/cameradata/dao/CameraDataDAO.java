package ru.kabakov.cameradata.dao;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import ru.kabakov.cameradata.dao.model.CameraDataModel;
import ru.kabakov.cameradata.dao.model.SourceDataModel;
import ru.kabakov.cameradata.dao.model.TokenDataModel;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
public class CameraDataDAO {
    private static final String SOURCE_URL = "http://www.mocky.io/v2/5c51b9dd3400003252129fb5";

    public List<CameraDataModel> getCameraDataModelList() {
        List<CameraDataModel> cameraDataModelList = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            cameraDataModelList = mapper.readValue(new URL(SOURCE_URL), new TypeReference<List<CameraDataModel>>() {});
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return cameraDataModelList;
    }

    public SourceDataModel getCameraSourceData(String url) {
        SourceDataModel sourceDataModel = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            sourceDataModel = mapper.readValue(new URL(url), SourceDataModel.class);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return sourceDataModel;
    }

    public TokenDataModel getCameraTokenData(String url) {
        TokenDataModel tokenDataModel = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            tokenDataModel = mapper.readValue(new URL(url), TokenDataModel.class);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return tokenDataModel;
    }
}