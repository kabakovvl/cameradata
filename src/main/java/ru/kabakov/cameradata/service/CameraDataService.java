package ru.kabakov.cameradata.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kabakov.cameradata.dao.CameraDataDAO;
import ru.kabakov.cameradata.dao.model.CameraDataModel;
import ru.kabakov.cameradata.service.converter.SourceDataModelToDTOConverter;
import ru.kabakov.cameradata.service.converter.TokenDataModelToDTOConverter;
import ru.kabakov.cameradata.service.dto.CameraResDataDTO;
import ru.kabakov.cameradata.service.dto.SourceDataDTO;
import ru.kabakov.cameradata.service.dto.TokenDataDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CameraDataService {
    private final CameraDataDAO cameraDataDAO;
    private final SourceDataModelToDTOConverter sourceDataModelToDTOConverter;
    private final TokenDataModelToDTOConverter tokenDataModelToDTOConverter;

    public List<CameraResDataDTO> getCameraResData() {
        List<CameraResDataDTO> cameraResDataDTOList = Collections.synchronizedList(new ArrayList<>());

        List<CameraDataModel> cameraDataModels = cameraDataDAO.getCameraDataModelList();

        cameraDataModels.parallelStream().forEach(camera -> {
            SourceDataDTO sourceDataDTO = sourceDataModelToDTOConverter
                    .convert(cameraDataDAO
                            .getCameraSourceData(camera.getSourceDataUrl()));
            TokenDataDTO tokenDataDTO = tokenDataModelToDTOConverter
                    .convert(cameraDataDAO
                            .getCameraTokenData(camera.getTokenDataUrl()));

            if (sourceDataDTO == null || tokenDataDTO == null) {
                return;
            }
            CameraResDataDTO cameraResDataDTO = new CameraResDataDTO()
                    .setId(camera.getId())
                    .setUrlType(sourceDataDTO.getUrlType())
                    .setVideoUrl(sourceDataDTO.getVideoUrl())
                    .setValue(tokenDataDTO.getValue())
                    .setTtl(tokenDataDTO.getTtl());

            cameraResDataDTOList.add(cameraResDataDTO);
        });

        return cameraResDataDTOList;
    }
}