package ru.kabakov.cameradata.service.dto;

import lombok.Data;

@Data
public class CameraDataDTO {

    private Long id;
    private SourceDataDTO sourceDataUrl;
    private TokenDataDTO tokenDataUrl;
}
