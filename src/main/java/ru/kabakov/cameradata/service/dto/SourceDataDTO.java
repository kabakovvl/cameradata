package ru.kabakov.cameradata.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.kabakov.cameradata.dao.model.UrlType;

@Data
@Accessors(chain = true)
public class SourceDataDTO {
    private UrlType urlType;
    private String videoUrl;
}
