package ru.kabakov.cameradata.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TokenDataDTO {
    private String value;
    private Integer ttl;
}
