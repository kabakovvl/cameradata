package ru.kabakov.cameradata.service.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.kabakov.cameradata.dao.model.TokenDataModel;
import ru.kabakov.cameradata.service.dto.TokenDataDTO;

@Component
public class TokenDataModelToDTOConverter implements Converter<TokenDataModel, TokenDataDTO> {
    @Override
    public TokenDataDTO convert(TokenDataModel tokenDataModel) {
        return new TokenDataDTO()
                .setValue(tokenDataModel.getValue())
                .setTtl(tokenDataModel.getTtl());
    }
}
