package ru.kabakov.cameradata.service.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.kabakov.cameradata.dao.model.SourceDataModel;
import ru.kabakov.cameradata.service.dto.SourceDataDTO;

@Component
public class SourceDataModelToDTOConverter implements Converter<SourceDataModel, SourceDataDTO> {
    @Override
    public SourceDataDTO convert(SourceDataModel sourceDataModel) {
        return new SourceDataDTO()
                .setUrlType(sourceDataModel.getUrlType())
                .setVideoUrl(sourceDataModel.getVideoUrl());
    }
}
