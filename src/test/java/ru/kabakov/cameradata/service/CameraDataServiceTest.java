package ru.kabakov.cameradata.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.kabakov.cameradata.dao.CameraDataDAO;
import ru.kabakov.cameradata.dao.model.CameraDataModel;
import ru.kabakov.cameradata.service.converter.SourceDataModelToDTOConverter;
import ru.kabakov.cameradata.service.converter.TokenDataModelToDTOConverter;
import ru.kabakov.cameradata.service.dto.CameraResDataDTO;
import ru.kabakov.cameradata.service.dto.SourceDataDTO;
import ru.kabakov.cameradata.service.dto.TokenDataDTO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CameraDataServiceTest {

    @InjectMocks
    CameraDataService subj;

    @Mock
    CameraDataDAO cameraDataDAO;
    @Mock
    SourceDataModelToDTOConverter sourceDataModelToDTOConverter;
    @Mock
    TokenDataModelToDTOConverter tokenDataModelToDTOConverter;

    private final List<CameraDataModel> cameraDataModelList = new ArrayList<>();

    @Before
    public void setUp() {

        cameraDataModelList.add(new CameraDataModel()
                .setId(1L)
                .setSourceDataUrl("http://www.mocky.io/v2/5c51b230340000094f129f5d")
                .setTokenDataUrl("http://www.mocky.io/v2/5c51b5b6340000554e129f7b?mocky-delay=1s")
        );
        cameraDataModelList.add(new CameraDataModel()
                .setId(2L)
                .setSourceDataUrl("http://www.mocky.io/v2/5c51b5023400002f4f129f70")
                .setTokenDataUrl("http://www.mocky.io/v2/5c51b623340000404f129f82")
        );

        when(cameraDataDAO.getCameraDataModelList()).thenReturn(cameraDataModelList);
    }

    @Test
    public void getCameraResData_twoCamerasReceived() {
        for (CameraDataModel cam : cameraDataModelList) {
            when(sourceDataModelToDTOConverter
                    .convert(cameraDataDAO
                            .getCameraSourceData(cam.getSourceDataUrl())))
                    .thenReturn(new SourceDataDTO());
            when(tokenDataModelToDTOConverter
                    .convert(cameraDataDAO
                            .getCameraTokenData(cam.getTokenDataUrl())))
                    .thenReturn(new TokenDataDTO());
        }

        List<CameraResDataDTO> cameraResDataDTOList = new ArrayList<>();

        cameraResDataDTOList.add(new CameraResDataDTO().setId(1L));
        cameraResDataDTOList.add(new CameraResDataDTO().setId(2L));

        List<CameraResDataDTO> cameraResDataDTOListAct = subj.getCameraResData();

        assertEquals(2, cameraResDataDTOListAct.size());
        assertTrue(cameraResDataDTOListAct.containsAll(cameraResDataDTOList));
        verify(cameraDataDAO, times(1)).getCameraDataModelList();
        verify(cameraDataDAO, times(2)).getCameraSourceData("http://www.mocky.io/v2/5c51b5023400002f4f129f70");
        verify(cameraDataDAO, times(2)).getCameraTokenData("http://www.mocky.io/v2/5c51b623340000404f129f82");
    }

    @Test
    public void getCameraResData_nullDataReceived() {
        for (CameraDataModel cam : cameraDataModelList) {
            when(sourceDataModelToDTOConverter
                    .convert(cameraDataDAO
                            .getCameraSourceData(cam.getSourceDataUrl())))
                    .thenReturn(null);
            when(tokenDataModelToDTOConverter
                    .convert(cameraDataDAO
                            .getCameraTokenData(cam.getTokenDataUrl())))
                    .thenReturn(null);
        }

        List<CameraResDataDTO> cameraResDataDTOListAct = subj.getCameraResData();

        assertTrue(cameraResDataDTOListAct.isEmpty());
        verify(cameraDataDAO, times(1)).getCameraDataModelList();
    }
}