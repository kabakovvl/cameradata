package ru.kabakov.cameradata.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kabakov.cameradata.dao.model.CameraDataModel;
import ru.kabakov.cameradata.dao.model.SourceDataModel;
import ru.kabakov.cameradata.dao.model.TokenDataModel;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CameraDataDAOTest {
    @Autowired
    CameraDataDAO subj;

    @Test
    public void getCameraDataModelList_notEmpty() {
        List<CameraDataModel> cameraDataModelList = subj.getCameraDataModelList();
        assertFalse(cameraDataModelList.isEmpty());
    }

    @Test
    public void getCameraSourceData_notNull() {
        SourceDataModel cameraSourceData = subj.getCameraSourceData("http://www.mocky.io/v2/5c51b5023400002f4f129f70");
        assertNotNull(cameraSourceData);
    }

    @Test
    public void getCameraTokenData_notNull() {
        TokenDataModel tokenDataModel = subj.getCameraTokenData("http://www.mocky.io/v2/5c51b623340000404f129f82");
        assertNotNull(tokenDataModel);
    }
}