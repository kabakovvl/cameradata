package ru.kabakov.cameradata.api.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kabakov.cameradata.service.CameraDataService;
import ru.kabakov.cameradata.service.dto.CameraResDataDTO;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.kabakov.cameradata.dao.model.UrlType.ARCHIVE;
import static ru.kabakov.cameradata.dao.model.UrlType.LIVE;

@WebMvcTest(CameraDataController.class)
@RunWith(SpringRunner.class)
public class CameraDataControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    CameraDataService cameraDataService;

    @Test
    public void getCameraResDataList_goodResult() throws Exception {
        List<CameraResDataDTO> cameraResDataDTOList = new ArrayList<>(asList(
                new CameraResDataDTO()
                        .setId(1L)
                        .setTtl(180)
                        .setUrlType(LIVE)
                        .setVideoUrl("rtsp://127.0.0.1/20")
                        .setValue("fa4b5f64-249b-11e9-ab14-d663bd873d93"),
                new CameraResDataDTO()
                        .setId(2L)
                        .setTtl(60)
                        .setUrlType(ARCHIVE)
                        .setVideoUrl("rtsp://127.0.0.1/2")
                        .setValue("fa4b5b22-249b-11e9-ab14-d663bd873d93")));

        when(cameraDataService.getCameraResData()).thenReturn(cameraResDataDTOList);

        mockMvc.perform(get("/getCameraResData"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"cameraResDataDTOList\": [\n" +
                        "    {\n" +
                        "      \"id\": 1,\n" +
                        "      \"urlType\": \"LIVE\",\n" +
                        "      \"videoUrl\": \"rtsp://127.0.0.1/20\",\n" +
                        "      \"value\": \"fa4b5f64-249b-11e9-ab14-d663bd873d93\",\n" +
                        "      \"ttl\": 180\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"id\": 2,\n" +
                        "      \"urlType\": \"ARCHIVE\",\n" +
                        "      \"videoUrl\": \"rtsp://127.0.0.1/2\",\n" +
                        "      \"value\": \"fa4b5b22-249b-11e9-ab14-d663bd873d93\",\n" +
                        "      \"ttl\": 60\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}"));
    }

    @Test
    public void getCameraResDataList_noResult() throws Exception {
        List<CameraResDataDTO> cameraResDataDTOList = new ArrayList<>();
        when(cameraDataService.getCameraResData()).thenReturn(cameraResDataDTOList);

        mockMvc.perform(get("/getCameraResData"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void getCameraResDataList_badRequest() throws Exception {
        when(cameraDataService.getCameraResData()).thenReturn(null);

        mockMvc.perform(get("/getCameraResData"))
                .andExpect(status().isBadRequest());
    }
}